# CreativeCR10_Tools

## ObjectifS:

	*Création d'outils pour CreativeCR10 3D printer


## Sous Objectif:

	*Utilisation de la CreativeCR10 comme d'une machine à dessiner à l'aide de stylot
	*Utilisation de la CreativeCR10 comme d'une fraiseuse à commande numerique à l'aide d'une dremel (model 4000 F013400046)
	*Utilisation de la CreativeCR10 comme d'un microscope acoustique
	*Utilisation d'un Raspberry Pi pour controler la CreativeCR10


## Compétance dévelopees:

	*Modélisation 3D
	*Programmation: C, C++, Shell 
	